package main

import (
	"log"
	"net/http"
)

// type ClientMange struct {
// 	clients map[*Client]bool
// }

// //Check if this user already in a hub with other user
// func (c ClientMange) checkUsername(u1 string, u2 string) {

// }

////////////////////
type HubManage struct {
	hubs map[*Hub]bool
	//what happend when a hub is deleted?
}

func (h *HubManage) addHub(newhub *Hub) {
	h.hubs[newhub] = true
}

//for binary hub only
func (h HubManage) checkUserInHub(u1 string, u2 string) *Hub {
	u1ok := false
	u2ok := false

	for hub, _ := range h.hubs {

		for client, _ := range hub.expectedClients {

			if client == u1 {
				u1ok = true
				continue
			}
			if client == u2 {
				u2ok = true
				continue
			}

		}
		if u1ok && u2ok {
			return hub
		}
	}
	return nil
}

//////////////
func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "home.html")
}

func main() {
	hubManage := &HubManage{
		hubs: make(map[*Hub]bool),
	}
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws/binary", func(w http.ResponseWriter, r *http.Request) {
		log.Println(r.URL)
		u1 := r.URL.Query().Get("u1")
		u2 := r.URL.Query().Get("u2")
		existedHub := hubManage.checkUserInHub(u1, u2)
		if existedHub == nil {
			log.Println("hub not found, create new hub")
			//make new hub for 2 users
			hub := newBinaryHub(u1, u2)
			go hub.run()
			hubManage.addHub(hub)
			serveWs(u1, hub, w, r)
		} else {
			log.Println("hub found, join")
			//connect?
			serveWs(u2, existedHub, w, r)
		}
	})
	// http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
	// 	serveWs(w, r)
	// })
	err := http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
